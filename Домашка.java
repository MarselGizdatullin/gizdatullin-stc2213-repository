
** Решение задач псевдокода

1. Найти минимальное число

public class Main {

    public static void main(String[] args) {
        int[] numbersArray = {23, 43, 89, 19, 29};
        int small =  numbersArray[0];
        for (int i = 0; i < numbersArray.length; i++) {
            for (int j = i+1; j < numbersArray.length; j++) {
                if(numbersArray[i] > numbersArray[j]) {
                    small = numbersArray[i];
                    numbersArray[i] = numbersArray[j];
                    numbersArray[j] = small;
                }
            }
            System.out.println("Наименьшее число: " + numbersArray[0]);





 2. Посчитать количество всех четных чисел и нечетных

 public class Main {

    public static void main(String[]args){
        int arr[]=new int[]{4,12,43,37,98,25,6};
        int n=0;
        int m=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]%2==0){
                n++;
            }
            else {
                m++;
            }
        }
        System.out.println ("четных чисел:" + n + " штуки");
        System.out.println ("нечетных чисел:" + m + " штуки");
    }
}           





**Дополнительние Задачи от наставника

1. Перевод из десятичной в двоичную

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите десятичное число и нажмите Enter");
        int n = scan.nextInt();
        String convert = Integer.toBinaryString(n);
        System.out.println("Бинарный код вашего числа " + convert);
        }
    }



2. Перевод из двоичной в десятичную


public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите любой бинарный код");
        String s = scan.next();
            System.out.println("Значение в десятичной системе счисления " + Long.parseLong(s, 2));
        }
    }


3. Не выполнил
_________________________________________________________

4. Определение простое число или нет

public class Main {

    public static boolean IsSimple(int value)
    {
        for (int i = 2; i < value; i++)
        {
            if (value % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args)
    {
        System.out.println(IsSimple(6));
    }
}


5. Вывести на экран все простые числа от 1 до N


public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter numer");
        int n = input.nextInt();
        for (int i = 0; i <= n; i++) {
            if (isCheck(i)) {
                System.out.println(i);
            }
        }
    }

    public static boolean isCheck(int value) {
        if (value % 2 == 0) return false;
        for (int i = 3; i * i <= value; i += 2) {
            if (value % i == 0)
                return false;
        }
        return true;
    }
}


